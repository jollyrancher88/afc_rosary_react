import 'react-native';
import React from 'react';
import RosaryContainer from '../../app/lib/RosaryContainer'
import FakeAPIResponse from '../__support__/get_all.json';

var MockDate = require('mockdate');

const mystery_mappings = {"Sunday" : "glorious",
						  "Monday" : "joyful",
						  "Tuesday" : "sorrowful",
						  "Wednesday" : "glorious",
						  "Thursday" : "luminous",
						  "Friday" : "sorrowful",
						  "Saturday" : "joyful"};

const days = Object.keys(mystery_mappings);
const current_day = new Date().getDay();
const current_day_mystery = mystery_mappings[days[current_day]];
const default_mystery = "luminous";

let rc = new RosaryContainer();
let rosary_data = FakeAPIResponse.data.rosaries;

describe('RosaryContainer', () => {
	describe('.todays_mystery()', () => {
		// For each day of the week
		days.forEach(function(day, i) {
			test('todays_mystery(' + day + ') => ' + mystery_mappings[day], () => {
				expect(rc.todays_mystery(i)).toEqual(mystery_mappings[day]);
			})
		});

		test('todays_mystery(current_day) => ' + current_day_mystery, () => {
			expect(rc.todays_mystery(current_day)).toEqual(current_day_mystery);
		})

		test('todays_mystery(100) => ' + default_mystery, () => {
			expect(rc.todays_mystery(100)).toEqual(default_mystery);
		})

		test('todays_mystery("blah") => ' + current_day_mystery, () => {
			expect(rc.todays_mystery("blah")).toEqual(current_day_mystery);
		})

		test('todays_mystery(null) => ' + current_day_mystery, () => {
			expect(rc.todays_mystery(null)).toEqual(current_day_mystery);
		})
	})

	describe('.constructor()', () => {
		describe('rosaries is [] ', () => {
			let rc = new RosaryContainer([]);
			test(' => []', () => {
				expect(rc.rosaries).toEqual([]);
			})
		})


		describe('rosaries is rosary_data from file', () => {
			let rc = new RosaryContainer(rosary_data);
			test(' => rosary_data', () => {
				expect(rc.rosaries).toEqual(rosary_data);
			})
		})

		describe('bad rosaries data', () => {
	    	let bad_values = [null,"", 1, -1, {}]

			bad_values.forEach(function(bad_value) {
					let rc = new RosaryContainer(bad_value);
					let bad_val_str = bad_value === "" ? '""' : String(bad_value)
					test('' + bad_val_str + ' => []', () => {
						expect(rc.rosaries).toEqual([]);
					});
			});
		})
	})

	describe('.getTodaysRosary()', () => {
		
		afterAll(() => {
			MockDate.reset();
		})

		let rc = new RosaryContainer(rosary_data);

		const day_dates = { "Sunday" : "2/12/2017",
							"Monday" : "2/13/2017",
							"Tuesday" : "2/14/2017",
							"Wednesday" : "2/15/2017",
							"Thursday" : "2/16/2017",
							"Friday" : "2/17/2017",
							"Saturday" : "2/18/2017" }

		const day_meditations = { "Sunday" : "glorious_mysteries",
							"Monday" : "joyful_mysteries",
							"Tuesday" : "sorrowful_mysteries",
							"Wednesday" : "glorious_mysteries",
							"Thursday" : "luminous_mysteries",
							"Friday" : "sorrowful_mysteries",
							"Saturday" : "joyful_mysteries" }

		describe('media_type is unsupported', () => {
			let media_type = 'image';
			test('returns fake rosary', () => {
				rosary = rc.getTodaysRosary(media_type);
				expect(rosary.id).toEqual('fake_id');
			})
		})

		days.forEach(function(day, i) {
			describe('current day is ' + day, () => {
				describe('media type is audio', () => {
					let media_type = 'audio';
					test('data checks out', () => {
						MockDate.set(day_dates[day]);
						rosary = rc.getTodaysRosary(media_type);
						expect(rosary.media_resource.mime).toEqual("audio/mpeg3");
						expect(rosary.meditation_type).toEqual(day_meditations[day]);
					})
		 		})
		 		describe('media type is video', () => {
					let media_type = 'video';
					test('data checks out', () => {
						MockDate.set(day_dates[day]);
						rosary = rc.getTodaysRosary(media_type);
						expect(rosary.media_resource.mime).toEqual("video/mp4");
						expect(rosary.meditation_type).toEqual(day_meditations[day]);
					})
		 		})	
			})
		});
	})
})

