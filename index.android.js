import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  Navigator,
  Dimensions,
  BackAndroid
} from 'react-native';

var RosaryMenu = require('./app/layouts/RosaryMenu/RosaryMenu.js');
var Learn = require('./app/layouts/Learn/Learn.js');
var Settings = require('./app/layouts/Settings/Settings.js');
var More = require('./app/layouts/More/More.js');

export default class afc_rosary_react extends Component {
 constructor(props) {
   super(props);
   this._pasEditUnmountFunction = this._pasEditUnmountFunction.bind(this);
 }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  _pasEditUnmountFunction() {
  }

 render() {
  var navigator;

  const routes = [ {name: 'Learn', index: 0}, {name: 'RosaryMenu', index: 1} ];

  BackAndroid.addEventListener('hardwareBackPress', () => {
    if ((!(navigator)) || navigator.getCurrentRoutes().length === 2  ) {
       return false;
    } else {
      if (navigator.getCurrentRoutes().length == 1) {
        navigator.push(routes[1]);
        return true;
      }
    }
    // navigator.pop();
    return true;
  });

  return (
     <Navigator
     ref={(nav) => { navigator = nav; }}
     configureScene={ this.configureScene }
      initialRoute={routes[1]}
      initialRouteStack={routes}
      renderScene={ this.renderScene }
     />
    );
  }

  configureScene(route, routeStack) {
  /* Available scene configutation options are:
      *  - Navigator.SceneConfigs.PushFromRight (default)
      *  - Navigator.SceneConfigs.FloatFromRight
      *  - Navigator.SceneConfigs.FloatFromLeft
      *  - Navigator.SceneConfigs.FloatFromBottom
      *  - Navigator.SceneConfigs.FloatFromBottomAndroid
      *  - Navigator.SceneConfigs.FadeAndroid
      *  - Navigator.SceneConfigs.HorizontalSwipeJump
      *  - Navigator.SceneConfigs.HorizontalSwipeJumpFromRight
      *  - Navigator.SceneConfigs.VerticalUpSwipeJump
      *  - Navigator.SceneConfigs.VerticalDownSwipeJump
   */
      switch(route.name) {
          case 'VideoPlayer':
          case 'AudioPlayer':
              return Navigator.SceneConfigs.FadeAndroid;
          case 'Learn':
              return Navigator.SceneConfigs.HorizontalSwipeJumpFromRight;
          case 'Settings':
          case 'RosaryMenu':
              return Navigator.SceneConfigs.HorizontalSwipeJump;
          case 'More':
              return Navigator.SceneConfigs.VerticalUpSwipeJump;
          default:
              return Navigator.SceneConfigs.FadeAndroid;
      }
  }

  renderScene(route, navigator) {
     if(route.name == 'RosaryMenu') {
       return <RosaryMenu navigator={navigator} />
     }
     if(route.name == 'More') {
       return <More style={{fontSize: 18 }} navigator={navigator}>More</More>
     }
     if(route.name == 'Learn') {
       return <Text style={{fontSize: 18 }} navigator={navigator}>Learn</Text>
     }
     if(route.name == 'Settings') {
       return <Settings style={{fontSize: 18 }} navigator={navigator}>Settings</Settings>
     }
  }
}

AppRegistry.registerComponent('afc_rosary_react', () => afc_rosary_react);
