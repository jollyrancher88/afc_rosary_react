/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  Image,
  View,
  TouchableHighlight,
  Dimensions
} from 'react-native';
import Button from 'react-native-button';

class SelectButton extends Component {
  render (text) {
    text_styles = this.props.text_styles || {fontSize: 32, color: 'white', textAlign: 'center'};

    return (
     <Button
      containerStyle={{padding:10, margin:5, height:60, minWidth:300,  borderRadius:1, backgroundColor: 'rgba(175, 30, 45, 0.5)'}}>
      <Text style={text_styles}>
        {this.props.text}
      </Text>
    </Button>
    )
  }
}

class afc_rosary_react extends Component {
  render() {
    var window = Dimensions.get('window');
    var favorite_button_styles = {fontSize: 32, fontWeight: 'bold', color: 'white', textAlign: 'center'};
    var btnFavorite = new SelectButton({text: 'Play FAVORITE', text_styles: favorite_button_styles});
    var btnAudio = new SelectButton({text: 'PLAY AUDIO'});
    var btnVideo = new SelectButton({text: 'PLAY VIDEO'});
    return (
      <View style={ {flex: 1, alignItems: 'center', marginTop: 20} }>
        <View style={styles.bgImageWrapper}>
         <Image source={require('./angel_background.png')} style={{width: window.width, height: window.height}} />
       </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'rgba(130, 133, 152, 0.5)', width: window.width, padding:10, margin: 0}}>
          <View style={{paddingLeft: 5}}>
            <Button style={{color: 'white'}}>LEARN</Button>
          </View>
          <View style={{}}>
            <Button style={{color: 'white'}}>SETTINGS </Button>
          </View>
        </View>
        <View style={{marginTop: 320}}>
          {btnFavorite.render()}
          {btnAudio.render()}
          {btnVideo.render()}
        </View>
        <View style={{paddingBottom: 0, marginTop: 30}}>
          <Button>
            <View style={{flexDirection: 'column', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0)'}}>
            <Text style={{marginBottom: -5, fontSize: 18 }}>More Options</Text>
            <Text style={{fontSize: 18 }}>V</Text>
            </View>
          </Button>
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  hello: {
    fontSize: 50,
    textAlign: 'center',
    margin: 10,
    color: 'red',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'stretch', // or 'cover'
  },
  bgImageWrapper: {
    position: 'absolute',
    top: 0, bottom: 0, left: 0, right: 0
  },
  bgImage: {
    flex: 1,
    resizeMode: "stretch"
  },
  button: {
    textAlign: 'center',
    color: '#ffffff',
    marginBottom: 7
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  }
});

AppRegistry.registerComponent('afc_rosary_react', () => afc_rosary_react);
