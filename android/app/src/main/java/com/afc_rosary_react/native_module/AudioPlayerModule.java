package org.afcapps.rosary;

import android.app.Activity;
import android.content.Intent;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ActivityEventListener;
import org.afcapps.rosary.data.Rosary;
import org.afcapps.rosary.data.AudioResource;
import org.afcapps.rosary.data.enums.MeditationType;
import org.afcapps.rosary.StreamingMp3Player;
import android.util.Log;

public class AudioPlayerModule extends ReactContextBaseJavaModule {
    public AudioPlayerModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    private Callback playerSuccessCallback;
    private Callback playerCancelCallback;

    @ReactMethod
    // public void playRosary(ReadableMap config, Callback successCallback, Callback cancelCallback) {
    public void playRosary() {
        Log.d("@ReactMethod","HERE in playRosary!");
        Activity currentActivity = getCurrentActivity();
        AudioResource audio = new AudioResource("", "https://www.dropbox.com/s/nctipayy63apoib/jerry_joyful_mysteries.mp3?dl=1", "Joyful Mysteries",false);
        Rosary audioRosary = new Rosary("", MeditationType.GLORIOUS_MYSTERIES, audio);


        if (currentActivity == null) {
            // cancelCallback.invoke("Activity doesn't exist");
            Log.d("@ReactMethod","Activity doesn't exist!");
            return;
        }

        // playerSuccessCallback = successCallback;
        // playerCancelCallback = cancelCallback;

        try {
            Log.d("@ReactMethod","Trying to launch audio rosary...");
            final Intent intent = new Intent(currentActivity, StreamingMp3Player.class);

            intent.putExtra("Rosary", audioRosary);
            currentActivity.startActivity(intent);
            Log.d("@ReactMethod","Done Launching Audio Rosary.");
        } catch (Exception e) {
            // cancelCallback.invoke(e);
            Log.d("@ReactMethod","Something went wrong! " + e.getMessage());
        }
    }

    @Override
    public String getName() {
        return "AudioPlayer";
    }

    // @Override 
    // public Map<String, Object> getConstants() { 
    //     final Map<String, Object> constants = new HashMap<>();
    //     // constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
    //     // constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
    //     return constants;
    // }

    // @Override
    // public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        // if (playerSuccessCallback != null) {
        //     if (resultCode == Activity.RESULT_CANCELED) {
        //         playerCancelCallback.invoke("ImagePicker was cancelled");
        //     } else if (resultCode == Activity.RESULT_OK) {
        //         Uri uri = intent.getData();

        //         if (uri == null) {
        //             playerCancelCallback.invoke("No image data found");
        //         } else {
        //             try {
        //                 playerSuccessCallback.invoke(uri);
        //             } catch (Exception e) {
        //                 playerCancelCallback.invoke("No image data found");
        //             }
        //         }
        //     }
        // }
    // }
}