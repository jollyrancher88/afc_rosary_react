/** This class has been ported from:
 *  https://github.com/ihrupin/samples/tree/master/android/Streaming_Audio_Player
 *  and adapted to work with this afcapps package
 */

package org.afcapps.rosary;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.util.Log;

import org.afcapps.rosary.data.Rosary;
import org.afcapps.rosary.service.Storage;
import com.afc_rosary_react.R;

import java.util.ArrayList;

public class StreamingVideoPlayer extends Activity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, OnClickListener, OnTouchListener, OnCompletionListener, OnBufferingUpdateListener{

    private ImageButton buttonPlayPause;
    private SeekBar seekBarProgress;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;

    private MediaPlayer mediaPlayer;
    private boolean mediaPlayerReleased;
    private int mediaFileLengthInMilliseconds; // this value contains the song duration in milliseconds. Look at getDuration() method in MediaPlayer class

    private final Handler handler = new Handler();
    private ProgressDialog progress;

    private Rosary Rosary;
    private String fileURL;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Intent intent = getIntent();

        Rosary = (Rosary)getIntent().getSerializableExtra("Rosary");
        if (Storage.rosaryMediaFileExists(this.getApplicationContext(), Rosary)) {
            //fileURL = Storage.rosaryMediaPath(this.getApplicationContext(), Rosary);
            fileURL = "https://www.dropbox.com/s/b7dt4uhsvj1t0tf/ben_joyful_mysteries.mp4?dl=1";
        } else {
            fileURL = Rosary.media.url;
            Log.d("StreamingVideoPlayer","fileURL : " + fileURL);
        }

        setContentView(R.layout.activity_play_video);
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Loading Video Rosary");
        progress.show();
        initView();
    }

    /** This method initialise all the views in project*/
    private void initView() {
        buttonPlayPause = (ImageButton)findViewById(R.id.ButtonTestPlayPause);
        buttonPlayPause.setOnClickListener(this);

        seekBarProgress = (SeekBar)findViewById(R.id.SeekBarTestPlay);
        seekBarProgress.setMax(99); // It means 100% .0-99
        seekBarProgress.setOnTouchListener(this);
        surfaceView = (SurfaceView) findViewById(R.id.SurfaceView);
        surfaceHolder = surfaceView.getHolder();
        surfaceView.setOnClickListener(this);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);

        try {
            mediaPlayer.setDataSource(fileURL); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                  @Override
                  public void onPrepared(MediaPlayer mp) {
                      finishInitView();
                  }
              });// you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            mediaPlayer.prepareAsync(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void finishInitView() {
        mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song count in milliseconds from URL

        resizeVideo();

        if(!mediaPlayer.isPlaying()){
            mediaPlayer.setDisplay(surfaceHolder);
            progress.dismiss();
            mediaPlayer.start();
            buttonPlayPause.setImageResource(R.drawable.button_pause);
        }else {
            mediaPlayer.pause();
            buttonPlayPause.setImageResource(R.drawable.button_play);
        }
        mediaPlayerReleased = false;
        primarySeekBarProgressUpdater();
    }

    private void resizeVideo() {
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        int screenHeight = getWindowManager().getDefaultDisplay().getHeight();
        android.view.ViewGroup.LayoutParams lp = surfaceView.getLayoutParams();
        RelativeLayout rl = (RelativeLayout) surfaceView.getParent();

        int currentWidth = mediaPlayer.getVideoWidth();
        int currentHeight = mediaPlayer.getVideoHeight();

        int videoWidth;
        int videoHeight;

        if (screenHeight > screenWidth) {
            float hwRatio = (float)  currentHeight / currentWidth;
            videoHeight = (int) (screenWidth * hwRatio);
            videoWidth = screenWidth;
            buttonPlayPause.setVisibility(View.VISIBLE);
            seekBarProgress.setVisibility(View.VISIBLE);
        } else {
            float whRatio = (float)  currentHeight / currentWidth;
            videoWidth = (int) (screenWidth);
            videoHeight = (int) (screenHeight * screenWidth);
            buttonPlayPause.setVisibility(View.INVISIBLE);
            seekBarProgress.setVisibility(View.INVISIBLE);

        }

        lp.width = videoWidth;
        lp.height = videoHeight;
        surfaceView.setLayoutParams(lp);
    }


    /** Method which updates the SeekBar primary progress by current song playing position*/
    private void primarySeekBarProgressUpdater() {
        if (mediaPlayerReleased == false) {
            seekBarProgress.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song count"
            if (mediaPlayer.isPlaying()) {
                Runnable notification = new Runnable() {
                    public void run() {
                        primarySeekBarProgressUpdater();
                    }
                };
                handler.postDelayed(notification, 1000);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        int screenHeight = getWindowManager().getDefaultDisplay().getHeight();

        if(v.getId() == R.id.ButtonTestPlayPause){
            /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
            try {
                mediaPlayer.setDataSource(fileURL); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (Exception e) {
                e.printStackTrace();
            }

            mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song count in milliseconds from URL

            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
                buttonPlayPause.setImageResource(R.drawable.button_pause);
                if (screenWidth > screenHeight) {
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    buttonPlayPause.setVisibility(View.INVISIBLE);
                                    seekBarProgress.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    };
                    thread.start(); //start the thread
                }
            }else {
                mediaPlayer.pause();
                buttonPlayPause.setImageResource(R.drawable.button_play);
            }

            mediaPlayerReleased = false;
            primarySeekBarProgressUpdater();
        } else if (v.getId() == R.id.SurfaceView) {
            if (buttonPlayPause.getVisibility() == View.VISIBLE && (screenWidth > screenHeight)) {
                buttonPlayPause.setVisibility(View.INVISIBLE);
                seekBarProgress.setVisibility(View.INVISIBLE);
            } else {
                buttonPlayPause.setVisibility(View.VISIBLE);
                seekBarProgress.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() == R.id.SeekBarTestPlay){
            /** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
            if(mediaPlayer.isPlaying()){
                SeekBar sb = (SeekBar)v;
                int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
                mediaPlayer.seekTo(playPositionInMillisecconds);
            }
        }
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        /** MediaPlayer onCompletion event handler. Method which calls then song playing is complete*/
        buttonPlayPause.setImageResource(R.drawable.button_play);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        /** Method which updates the SeekBar secondary progress by current song loading from URL position*/
        seekBarProgress.setSecondaryProgress(percent);
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mediaPlayerReleased = true;
        mediaPlayer.release();
        super.onDestroy();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        resizeVideo();
    }
}

