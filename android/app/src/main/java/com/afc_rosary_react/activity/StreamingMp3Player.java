/** This class has been ported from:
 *  https://github.com/ihrupin/samples/tree/master/android/Streaming_Audio_Player
 *  and adapted to work with this afcapps package
 */

package org.afcapps.rosary;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;
import com.afc_rosary_react.R;

import org.afcapps.rosary.data.Rosary;
import org.afcapps.rosary.service.Storage;

public class StreamingMp3Player extends Activity implements OnClickListener, OnTouchListener, OnCompletionListener, OnBufferingUpdateListener{

    private ImageButton buttonPlayPause;
    private SeekBar seekBarProgress;
//    public EditText editTextSongURL;

    private MediaPlayer mediaPlayer;
    private boolean mediaPlayerReleased;
    private int mediaFileLengthInMilliseconds; // this value contains the song duration in milliseconds. Look at getDuration() method in MediaPlayer class

    private final Handler handler = new Handler();
    private ProgressDialog progress;

    private Rosary Rosary;
    private String fileURL;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        Rosary = (Rosary)getIntent().getSerializableExtra("Rosary");
        if (Storage.rosaryMediaFileExists(this.getApplicationContext(), Rosary)) {
        // if (false) {
            fileURL = Storage.rosaryMediaPath(this.getApplicationContext(), Rosary);
        } else {
            fileURL = Rosary.media.url;
        }

        setContentView(R.layout.activity_play_audio);
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Loading Audio Rosary");
        progress.show();
//        Toast toast = Toast.makeText(this, "Playing Rosary..." + fileURL, Toast.LENGTH_LONG);
//        toast.show();
        initView();
    }

    /** This method initialise all the views in project*/
    private void initView() {
        buttonPlayPause = (ImageButton)findViewById(R.id.ButtonTestPlayPause);
        buttonPlayPause.setOnClickListener(this);

        seekBarProgress = (SeekBar)findViewById(R.id.SeekBarTestPlay);
        seekBarProgress.setMax(99); // It means 100% .0-99
        seekBarProgress.setOnTouchListener(this);
//        editTextSongURL = (EditText)findViewById(R.id.EditTextSongURL);
//        editTextSongURL.setText(R.string.test_mp3_file);
//        editTextSongURL.setText(fileURL);

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);


        try {
//            mediaPlayer.setDataSource(editTextSongURL.getText().toString()); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
            mediaPlayer.setDataSource(fileURL); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                                  @Override
                                                  public void onPrepared(MediaPlayer mp) {
                                                      finishInitView();
                                                  }
                                              });// you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            mediaPlayer.prepareAsync(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void finishInitView() {
        mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song count in milliseconds from URL

        if(!mediaPlayer.isPlaying()){
            progress.dismiss();
            mediaPlayer.start();
            buttonPlayPause.setImageResource(R.drawable.button_pause);
        }else {
            mediaPlayer.pause();
            buttonPlayPause.setImageResource(R.drawable.button_play);
        }
        mediaPlayerReleased = false;
        primarySeekBarProgressUpdater();
    }


    /** Method which updates the SeekBar primary progress by current song playing position*/
    private void primarySeekBarProgressUpdater() {
        if (mediaPlayerReleased == false) {
            seekBarProgress.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100)); // This math construction give a percentage of "was playing"/"song count"
            if (mediaPlayer.isPlaying()) {
                Runnable notification = new Runnable() {
                    public void run() {
                        primarySeekBarProgressUpdater();
                    }
                };
                handler.postDelayed(notification, 1000);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.ButtonTestPlayPause){
            /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
            try {
                mediaPlayer.setDataSource(fileURL); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (Exception e) {
                e.printStackTrace();
            }

            mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song count in milliseconds from URL

            if(!mediaPlayer.isPlaying()){
                mediaPlayer.start();
                buttonPlayPause.setImageResource(R.drawable.button_pause);
            }else {
                mediaPlayer.pause();
                buttonPlayPause.setImageResource(R.drawable.button_play);
            }

            mediaPlayerReleased = false;
            primarySeekBarProgressUpdater();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() == R.id.SeekBarTestPlay){
            /** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
            if(mediaPlayer.isPlaying()){
                SeekBar sb = (SeekBar)v;
                int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
                mediaPlayer.seekTo(playPositionInMillisecconds);
            }
        }
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        /** MediaPlayer onCompletion event handler. Method which calls then song playing is complete*/
        buttonPlayPause.setImageResource(R.drawable.button_play);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        /** Method which updates the SeekBar secondary progress by current song loading from URL position*/
        seekBarProgress.setSecondaryProgress(percent);
    }


    @Override
    protected void onStop() {

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mediaPlayerReleased = true;
        mediaPlayer.release();
        super.onDestroy();
    }
}

