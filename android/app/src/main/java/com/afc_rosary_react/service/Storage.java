package org.afcapps.rosary.service;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.afcapps.rosary.data.Rosary;

import java.io.File;

public class Storage {

    public static String ROSARY_DATA_FILE_NAME = "rosary_data.json";

    public Storage() {

    }

    public Storage(Storage RosaryAPI) {

    }

    private String rosary_json = "";

    private String getRosaryJSON() {
        Log.d("File", "here in the service");
        return this.rosary_json;
    }

    public String getRosaryData() {
        if (rosary_json.equals("")) {
            return getRosaryJSON();
        } else {
            return rosary_json;
        }
    }

    public String getLocalMediaPath(Rosary rosary) {
        String filePath = determineMediaPath(rosary);
        File rosaryMedia = new File(filePath);
        if (rosaryMedia.exists()) {
            return filePath;
        } else {
            return "";
        }
    }

    private String getLocalMediaPath() {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/media");
        return  direct.toString();
    }

    public File getMediaDirFile() {
        return new File(Environment.getExternalStorageDirectory() + "/media");
    }

    public File getAppDataDirFile() {
        return new File(Environment.getExternalStorageDirectory() + "/data");
    }

    public static File getMediaDirFile(Context c) {
        File mediaDirect = c.getExternalFilesDir("/media");
        if (mediaDirect.exists() == false) {
            mediaDirect.mkdir();
        }
        return mediaDirect;
    }

    public static File getAppDataDirFile(Context c) {
        File mediaDirect = c.getExternalFilesDir("/data");
        if (mediaDirect.exists() == false) {
            mediaDirect.mkdir();
        }
        return mediaDirect;
    }

    public static String rosaryMediaPath(Context c, Rosary rosary) {
        String path = "";
        String fileName = rosary.media.id + "." + parseFileExtension(rosary.media.url);
        path = getMediaDirFile(c).getAbsolutePath() + "/" + fileName;
        File pathFile = new File(path);

        if (pathFile.exists() == false) {
            return "";
        } else {
            return path;
        }
    }

    public static String rosaryDataPath(Context c) {
        String path = "";
        String fileName = ROSARY_DATA_FILE_NAME;
        path = getAppDataDirFile(c).getAbsolutePath() + "/" + fileName;
        File pathFile = new File(path);

        if (pathFile.exists() == false) {
            return "";
        } else {
            return path;
        }
    }

    public static boolean rosaryMediaFileExists(Context c, Rosary rosary) {
        return !(rosaryMediaPath(c, rosary).equals(""));
    }

    public static boolean rosaryDataFileExists(Context c) {
        return !(rosaryDataPath(c).equals(""));
    }

    public String determineMediaPath(Rosary rosary) {
        String fileExt = parseFileExtension(rosary.media.url);
        String filePath = getLocalMediaPath() + "/" + rosary.media.id + fileExt;
        return filePath;
    }

    public static String parseFileExtension(String url) {
        String ext = "";

        //  "url": "https://www.dropbox.com/s/kxu4ssc5pfmmu7z/afc-logo.png?dl=1"
        String[] pieces = url.split("\\?", 2);
        if (pieces.length == 2) {
            //  "piece": "https://www.dropbox.com/s/kxu4ssc5pfmmu7z/afc-logo.png"
            String piece = pieces[0];
            pieces = piece.split("/", 20);
            //  "piece": "afc-logo.png"
            piece = pieces[pieces.length -1];
            pieces = piece.split("\\.",2);
            //  "ext": "png"
            ext = pieces[pieces.length -1];
        }

        return ext;
    }

//    public org.afcapps.rosary.data.RosaryFactory getRosaryForToday()
    

}
