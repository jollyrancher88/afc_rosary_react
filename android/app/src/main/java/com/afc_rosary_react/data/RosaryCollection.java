package org.afcapps.rosary.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import org.afcapps.rosary.data.enums.MeditationType;
import org.afcapps.rosary.data.enums.ResourceType;
import org.apache.commons.collections4.*;

/**
 * Created by david.christensen on 8/26/15.
 */
public class RosaryCollection {
    private ArrayList<Rosary> List = new ArrayList<Rosary>();
    public final int count;

    public RosaryCollection(Rosary[] aRosaries) {
        List = new ArrayList<Rosary>(Arrays.asList(aRosaries));
        count = aRosaries.length;
    }

    public RosaryCollection(ArrayList<Rosary> alRosaries) {
        List = alRosaries;
        count = alRosaries.size();
    }

    public Rosary quickVideoRosary() {
        return prominentRosary(mediaVideoPredicate, predicateForTodaysRosary());
    }

    public Rosary quickAudioRosary() {
        return prominentRosary(mediaAudioPredicate, predicateForTodaysRosary());
    }

    private Predicate predicateForTodaysRosary() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:
                return gloriousMeditationPredicate;
            case Calendar.MONDAY:
                return joyfulMeditationPredicate;
            case Calendar.TUESDAY :
                return sorrowfulMeditationPredicate;
            case Calendar.WEDNESDAY:
                return gloriousMeditationPredicate;
            case Calendar.THURSDAY:
                return luminousMeditationPredicate;
            case Calendar.FRIDAY:
                return sorrowfulMeditationPredicate;
            default:
                return luminousMeditationPredicate;
        }
    }

    public Rosary getByID(String ID) {
        Rosary rosary = null;
        for (int i = 0; i < List.size(); i++) {
            Rosary r = List.get(i);
            if (r.id.equals(ID)) {
                rosary = copyRosary(r);
                break;
            }
        }
        return rosary;
    }

    private Predicate<Rosary> isAudioRosary = new Predicate<Rosary>() {
        @Override
        public boolean evaluate(Rosary rosary) {
            return rosary.media.type.equals(ResourceType.AUDIO);
        }
    };

    private Predicate<Rosary> isVideoRosary = new Predicate<Rosary>() {
        @Override
        public boolean evaluate(Rosary rosary) {
            return rosary.media.type.equals(ResourceType.VIDEO);
        }
    };

    private Predicate<Rosary> sorrowfulMeditationPredicate = new Predicate<Rosary>() {
        @Override
        public boolean evaluate(Rosary rosary) {
            return rosary.meditationType.equals(MeditationType.SORROWFUL_MYSTERIES);
        }
    };

    private Predicate<Rosary> gloriousMeditationPredicate = new Predicate<Rosary>() {
        @Override
        public boolean evaluate(Rosary rosary) {
            return rosary.meditationType.equals(MeditationType.GLORIOUS_MYSTERIES);
        }
    };

    private Predicate<Rosary> joyfulMeditationPredicate = new Predicate<Rosary>() {
        @Override
        public boolean evaluate(Rosary rosary) {
            return rosary.meditationType.equals(MeditationType.JOYFUL_MYSTERIES);
        }
    };

    private Predicate<Rosary> luminousMeditationPredicate = new Predicate<Rosary>() {
        @Override
        public boolean evaluate(Rosary rosary) {
            return rosary.meditationType.equals(MeditationType.LUMINOUS_MYSTERIES);
        }
    };


    private Predicate<Rosary> mediaVideoPredicate = new Predicate<Rosary>() {
        @Override
        public boolean evaluate(Rosary rosary) {
            return rosary.media.type.equals(ResourceType.VIDEO);
        }
    };

    private Predicate<Rosary> mediaAudioPredicate = new Predicate<Rosary>() {
        @Override
        public boolean evaluate(Rosary rosary) {
            return rosary.media.type.equals(ResourceType.AUDIO);
        }
    };

    class ProminenceComparitor implements Comparator<Rosary> {
        @Override
        public int compare(Rosary lhs, Rosary rhs) {
            return ((Rosary) lhs).prominanceLevel.compareTo(((Rosary) rhs).prominanceLevel);
        }
    }

    public void sortByProminence() {
        Collections.sort(List, new ProminenceComparitor());
    }

    private Rosary prominentRosary(Predicate mediaType, Predicate meditationType) {
        ArrayList<Rosary> rList = copyRosaryList(List);
        Integer count = CollectionUtils.countMatches(rList, mediaType);
        if (count > 0) {
            CollectionUtils.filter(rList,mediaType);
            count = CollectionUtils.countMatches(rList, meditationType);
            if (count > 0) {
                CollectionUtils.filter(rList, meditationType);
            }
            if (rList.size() > 1) {
                Collections.sort(rList, new ProminenceComparitor());
            }
            return rList.get(0);
        } else {
            return new Rosary();
        }
    }

    public ArrayList<Rosary> joyfulRosaries() {
        ArrayList<Rosary> alRosaries = copyRosaryList(List);
        CollectionUtils.filter(alRosaries, joyfulMeditationPredicate);
        return alRosaries;
    }

    public ArrayList<Rosary> allRosaries() {
        ArrayList<Rosary> alRosaries = copyRosaryList(List);
        return alRosaries;
    }

    public static ArrayList<Rosary> copyRosaryList(ArrayList<Rosary> rosaries) {
        ArrayList<Rosary> result = new ArrayList<Rosary>();
        for (Rosary rosary:
             rosaries) {
            Rosary tmpRosary = copyRosary(rosary);
            result.add(tmpRosary);
        }
        return result;
    }

    /**
     * Returns a copy of the rosary
     */
    public static Rosary copyRosary(Rosary orig) {
        Rosary obj = null;
        try {
            // Write the object out to a byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(orig);
            out.flush();
            out.close();

            // Make an input stream from the byte array and read
            // a copy of the object back in.
            ObjectInputStream in = new ObjectInputStream(
                    new ByteArrayInputStream(bos.toByteArray()));
            obj = (Rosary) in.readObject();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        catch(ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
        return obj;
    }
}
