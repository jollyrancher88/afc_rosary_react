package org.afcapps.rosary.data;

import org.afcapps.rosary.data.enums.ResourceType;

public class ImageResource extends MediaResource implements java.io.Serializable{

    public ImageResource() {
        super("", ResourceType.IMAGE, "fake_image_icon_location", "fake_caption", false);
    }

    public ImageResource(String id, String url, String caption, boolean isLocal) {
        super(id, ResourceType.IMAGE, url, caption, isLocal);
    }
}