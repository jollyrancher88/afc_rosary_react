package org.afcapps.rosary.data.enums;

public enum ResourceType {
    AUDIO,
    IMAGE,
    VIDEO,
    OTHER
}
