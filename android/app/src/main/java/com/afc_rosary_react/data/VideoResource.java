package org.afcapps.rosary.data;

import org.afcapps.rosary.data.enums.ResourceType;

public class VideoResource extends MediaResource implements java.io.Serializable {

    public String stream;

    public VideoResource(String id, String location, String caption, boolean isLocal) {
        super(id, ResourceType.VIDEO, location, caption, isLocal);
        stream = "";
    }

    public VideoResource(String id, String location, String stream, String caption, boolean isLocal) {
        super(id, ResourceType.VIDEO, location, caption, isLocal);
        this.stream = stream;
    }
}