package org.afcapps.rosary.data;

import org.afcapps.rosary.data.enums.MeditationType;

public class Rosary implements java.io.Serializable{
    public String id;
    public ImageResource icon = new ImageResource();
    public MediaResource media;
    public MeditationType meditationType;
    public Integer prominanceLevel;
    public String title;
    public String displayText;
    public Contributor[] contributors;

    public Rosary(String id, String title, ImageResource icon, MediaResource media, MeditationType meditationType, Integer prominanceLevel) {
        this.id = id;
        this.title = title;
        this.displayText = title;
        this.icon = icon;
        this.media = media;
        this.meditationType = meditationType;
        this.prominanceLevel = prominanceLevel;
        this.contributors = new Contributor[0];
    }

    public Rosary(String id, String title, String displayText, ImageResource icon, MediaResource media, MeditationType meditationType, Integer prominanceLevel, Contributor[] contributors) {
        this.id = id;
        this.title = title;
        this.displayText = displayText;
        this.icon = icon;
        this.media = media;
        this.meditationType = meditationType;
        this.prominanceLevel = prominanceLevel;
        this.contributors = contributors;
    }

    public Rosary() {
        this.id = "4";
        this.meditationType = MeditationType.GLORIOUS_MYSTERIES;
        this.media = new AudioResource("", "https://www.dropbox.com/s/nctipayy63apoib/jerry_joyful_mysteries.mp3?dl=1", "Joyful Mysteries", false);
        this.contributors = new Contributor[0];
    }

    public Rosary(MeditationType meditationType) {
        this.id = "3";
        this.meditationType = meditationType;
        this.media = new AudioResource("", "https://www.dropbox.com/s/nctipayy63apoib/jerry_joyful_mysteries.mp3?dl=1", "Joyful Mysteries", false);
        this.contributors = new Contributor[0];
    }

    public Rosary(String id, MeditationType meditationType) {
        this.id = "2";
        this.meditationType = meditationType;
        this.media = new AudioResource("", "https://www.dropbox.com/s/nctipayy63apoib/jerry_joyful_mysteries.mp3?dl=1", "Joyful Mysteries", false);
        this.contributors = new Contributor[0];
    }

    public Rosary(String id, MeditationType meditationType, MediaResource media) {
        this.id = "1";
        this.meditationType = meditationType;
        this.media = media;
        this.contributors = new Contributor[0];
    }

    public String getTitle() {
        return title;
    }

    public String setTitle(String title) {
        this.title = title;
        return this.title;
    }
}