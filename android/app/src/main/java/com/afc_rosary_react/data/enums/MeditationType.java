package org.afcapps.rosary.data.enums;

public enum MeditationType implements java.io.Serializable{
    JOYFUL_MYSTERIES,
    SORROWFUL_MYSTERIES,
    GLORIOUS_MYSTERIES,
    LUMINOUS_MYSTERIES,
    OTHER
}
