package org.afcapps.rosary.data;

import org.afcapps.rosary.data.enums.ResourceType;

public class MediaResource implements java.io.Serializable {
    public String id;
    public ResourceType type;
    public boolean isLocal;
    public String url;
    public String caption;

    public MediaResource(String id, ResourceType type, String location, String caption, boolean isLocal) {
        this.id = id;
        this.type = type;
        this.caption = caption;
        this.isLocal = isLocal;
        this.url = location;
    }

    public String getCaption() {
        return caption;
    }

    public String setCaption(String caption) {
        this.caption = caption;
        return this.caption;
    }
}