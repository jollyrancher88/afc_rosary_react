package org.afcapps.rosary.data;

/**
 * Created by david.christensen on 2/18/16.
 */
public class Contributor implements java.io.Serializable {
    public String id;
    public ImageResource photo = new ImageResource();
    public String firstName;
    public String lastName;
    public String fullName;

    public Contributor() {
        this.id = null;
        this.photo = new ImageResource();
        this.firstName = "";
        this.lastName = "";
        this.fullName = "";
    }

    public Contributor(String id, String firstName, String lastName) {
        this.id = id;
        this.photo = new ImageResource();
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = firstName + " " + lastName;
    }

    public Contributor(String id, String firstName, String lastName, String fullName) {
        this.id = id;
        this.photo = new ImageResource();
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
    }

    public Contributor(String id, String firstName, String lastName, String fullName, ImageResource photo) {
        this.id = id;
        this.photo = photo;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = fullName;
    }
}
