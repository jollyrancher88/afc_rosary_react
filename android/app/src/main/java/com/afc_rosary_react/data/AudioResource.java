package org.afcapps.rosary.data;

import org.afcapps.rosary.data.enums.ResourceType;

public class AudioResource extends MediaResource implements java.io.Serializable {

    public AudioResource(String id, String location, String caption, boolean isLocal) {
        super(id, ResourceType.AUDIO, location, caption, false);
    }
}