import React, { Component } from 'react';

class RosaryContainer extends Component {
  constructor(rosaries) {
    super();

    // rosaries must be an array of objects
    if (Object.prototype.toString.call( rosaries ) === '[object Array]') {
      if (Object.prototype.toString.call(rosaries[0]) == '[object Object]') {
        this.rosaries = rosaries;
      }
    }

    this.rosaries = this.rosaries || [];
  }

  todays_mystery(current_day) {
    current_day = (current_day === parseInt(current_day, 10)) ? current_day : new Date().getDay();

    switch(current_day) {
      case 0:
      case 3:
        return 'glorious';
      case 1:
      case 6:
        return 'joyful';
      case 2:
      case 5:
        return 'sorrowful';
      default:
        return 'luminous';
    }
  }

  getTodaysRosary(media_type) {
    if (!(this.rosaries.length > 0) || !media_type) return this.fakeRosary();
    var meditation_type = this.todays_mystery() + '_mysteries';
    this.rosaries = this.rosaries.sort(this.byProminence);
    for (var i = 0; i < this.rosaries.length; i++) {
      current_rosary = this.rosaries[i];
      if (this.matches_media_type(current_rosary, media_type) && current_rosary.meditation_type == meditation_type) {
        return current_rosary;
      }
    }
    return this.fakeRosary();
  }

  matches_media_type(rosary, media_type) {
    return rosary.media_resource.mime.split('/')[0] == media_type;
  }

  byProminence(a, b) {
    if (a.prominence_level < b.prominence_level)
      return -1;
    if (a.prominence_level > b.prominence_level)
      return 1;
    return 0;
  }

  fakeRosary() {
      return {
        "id": "fake_id",
        "title": "fake_title",
        "display_text": "fake_display_text",
        "meditation_type": "fake_meditation_type",
        "prominence_level": "fake_prominence_level",
        "contributors": [
            {
                "id": "fake_id",
                "full_name": "fake_full_name",
                "first_name": "fake_first_name",
                "last_name": "fake_last_name",
                "photo": {
                    "id": "fake_id",
                    "mime": "fake_mime",
                    "url": "fake_url",
                    "caption": "fake_caption"
                }
            }
        ],
        "icon": {
            "id": "fake_id",
            "mime": "fake_mime",
            "url": "fake_url",
            "caption": "fake_caption"
        },
        "media_resource": {
            "id": "fake_id",
            "caption": "fake_caption",
            "mime": "fake_mime",
            "url": "fake_url",
            "icon": {
                "id": "fake_id",
                "mime": "fake_mime",
                "url": "fake_url",
                "caption": "fake_caption"
            }
        }
    }
  }
}

module.exports = RosaryContainer;