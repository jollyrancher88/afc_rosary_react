import React, { Component } from 'react';
import {
  NativeModules
} from 'react-native';

class RosayAPI extends Component {
  constructor(fetch) {
    super();
    this.fetch_url = 'https://afc-rosary-API.herokuapp.com/v1/rosary/get_all';
  }

  fetchRosariesAsync() {
    return fetch(this.fetch_url)
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status == 'success') {
          return responseJson.data.rosaries;
        } else {
          let error = new Error('Bad Response from RosaryAPI');
          error.response = responseJson;
          throw error;
        }
      })
      .catch((error) => {
        console.error(error);
        throw error;
      });
  }
}

module.exports = RosayAPI;