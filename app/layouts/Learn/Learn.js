import { Text, BackAndroid } from 'react-native';

class Learn extends Text {
    constructor(props) {
        super(props);
        this._pasEditUnmountFunction = this._pasEditUnmountFunction.bind(this);
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this._pasEditUnmountFunction);
    }

    _pasEditUnmountFunction() {
        BackAndroid.removeEventListener('hardwareBackPress', this._pasEditUnmountFunction);
        this.props.navigator.push({name: 'RosaryMenu'});
    }
}

module.exports = Learn;