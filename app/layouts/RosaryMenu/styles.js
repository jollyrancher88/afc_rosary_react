import { StyleSheet } from 'react-native';

var RosaryMenuStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  hello: {
    fontSize: 50,
    textAlign: 'center',
    margin: 10,
    color: 'red',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'stretch', // or 'cover'
  },
  bgImageWrapper: {
    position: 'absolute',
    top: 0, bottom: 0, left: 0, right: 0
  },
  bgImage: {
    flex: 1,
    resizeMode: "stretch"
  },
  button: {
    textAlign: 'center',
    color: '#ffffff',
    marginBottom: 7
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  }
});

module.exports = RosaryMenuStyles;