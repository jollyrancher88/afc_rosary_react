import React, { Component } from 'react';
import {
  Text,
  Image,
  View,
  Dimensions,
  NativeModules
} from 'react-native';

import Button from 'react-native-button';

var SelectButton = require('../../components/SelectButton/SelectButton.js');
var PlayerButton = require('../../components/PlayerButton/PlayerButton.js');
var RosaryAPI = require('../../lib/RosaryAPI.js');
var RosaryContainer = require('../../lib/RosaryContainer.js');

class RosaryMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {rosaryContainer: new RosaryContainer() };
  }

  componentDidMount() { 
    this.fetchRosariesAsync(); 
  }

  fetchRosariesAsync() {
    return fetch('https://afc-rosary-API.herokuapp.com/v1/rosary/get_all')
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status == 'success') {
          this.setState({rosaryContainer: new RosaryContainer(responseJson.data.rosaries)});
          return true;
        } else {
          let error = new Error('Bad Response from RosaryAPI');
          error.response = responseJson;
          throw error;
        }
      })
      .catch((error) => {
        console.error(error);
        throw error;
      });
  }

  render() {
    var styles = require('./styles.js');
    var images = require('../../config/images.js');
    var window = Dimensions.get('window');
    var favorite_button_styles = {fontSize: 32, fontWeight: 'bold', color: 'white', textAlign: 'center'};
    var btnFavorite = new SelectButton({text: 'Play FAVORITE', text_styles: favorite_button_styles});
    var btnAudio = new PlayerButton('PLAY AUDIO', NativeModules.AudioPlayer);
    var btnVideo = new PlayerButton('PLAY VIDEO', NativeModules.VideoPlayer);
    let video_rosary = this.state.rosaryContainer.getTodaysRosary('video');

    return (
      <View style={ {flex: 1, alignItems: 'center'} }>
        <View style={styles.bgImageWrapper}>
         <Image source={images.backgrounds.angel} style={{width: window.width, height: window.height}} />
       </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'rgba(130, 133, 152, 0.5)', width: window.width, padding:10, margin: 0}}>
          <View style={{paddingLeft: 5}}>
            <Button style={{color: 'white'}} onPress={() => this.props.navigator.pop() }>LEARN</Button>
          </View>
          <View style={{}}>
            <Button style={{color: 'white'}}
                    onPress={() => this.props.navigator.push({name: 'Settings', index: 2}) }>
            SETTINGS </Button>
          </View>
        </View>
        <View style={{marginTop: 250}}>
          {btnFavorite.render(video_rosary.title)}
          {btnAudio.render()}
          {btnVideo.render()}
        </View>
        <View style={{paddingBottom: 0, marginTop: 10}}>
          <Button onPress={() => this.props.navigator.push({name: 'More', index: 2}) }>
            <View style={{flexDirection: 'column', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0)'}}>
            <Text style={{marginBottom: -5, fontSize: 18 }}>More Options</Text>
            <Text style={{fontSize: 18 }}>V</Text>
            </View>
          </Button>
        </View>
      </View>
    );
  }
}

module.exports = RosaryMenu;
