import React, { Component } from 'react';
import { Text } from 'react-native';
import Button from 'react-native-button';

class SelectButton extends Component {
  render (text) {
    text_styles = this.props.text_styles || {fontSize: 32, color: 'white', textAlign: 'center'};
    on_press = function() { alert('To be implemented...' + text) };  
    return (
     <Button
      onPress={() => on_press()}
      containerStyle={{padding:10, margin:5, height:60, minWidth:300,  borderRadius:1, backgroundColor: 'rgba(175, 30, 45, 0.5)'}}>
      <Text style={text_styles}>
        {this.props.text}
      </Text>
    </Button>
    )
  }
}

module.exports = SelectButton;
