import React, { Component } from 'react';
import { Text } from 'react-native';
import Button from 'react-native-button';

class PlayerButton extends Component {
  constructor(text, player) {
    super();
    this.text = text;
    this.player = player || null;
    this.text_styles = {fontSize: 32, color: 'white', textAlign: 'center'};
  }
  play() {
    if (this.player == null) {
      return;
    } else {
      this.player.playRosary();
    }
  }
  render (text) {
    return (
     <Button
      onPress={() => this.play()}
      containerStyle={{padding:10, margin:5, height:60, minWidth:300,  borderRadius:1, backgroundColor: 'rgba(175, 30, 45, 0.5)'}}>
      <Text style={this.text_styles}>
        {this.text}
      </Text>
    </Button>
    )
  }
}

module.exports = PlayerButton;